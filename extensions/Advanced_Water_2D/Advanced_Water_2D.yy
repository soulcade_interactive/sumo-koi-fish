{
    "id": "97bac8c8-24e9-4604-8726-1d998073e89c",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Advanced_Water_2D",
    "IncludedResources": [
        "Sprites\\spr_fish",
        "Sprites\\spr_ripple",
        "Sprites\\spr_sheet_tree",
        "Backgrounds\\bg_rocks",
        "Backgrounds\\bg_normals_water",
        "Scripts\\gfx_water_2d.gml",
        "Shaders\\shd_water_2d.shader",
        "Objects\\obj_game",
        "Objects\\obj_fish",
        "Objects\\obj_sheet_tree",
        "Objects\\obj_water",
        "Objects\\obj_ripple",
        "Rooms\\room_water"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2018-32-04 12:07:01",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.ABitten.AdvancedWater2D",
    "productID": "F79C97955FD1AF4686EB4EF4EF90F0DB",
    "sourcedir": "",
    "version": "1.0.0"
}