{
    "id": "714406c2-87bd-4ed7-ba85-3ce5596e75f0",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Super_Smash_Cam",
    "IncludedResources": [
        "Sprites\\spr_COM",
        "Sprites\\spr_Camera",
        "Sprites\\spr_Player",
        "Backgrounds\\bg_Grid",
        "Fonts\\fnt_COM",
        "Objects\\obj_Controller",
        "Objects\\obj_Player",
        "Rooms\\rm_Room"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2018-47-22 12:06:38",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.local306.supersmashcam",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "version": "1.0.0"
}