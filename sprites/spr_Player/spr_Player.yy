{
    "id": "9e4b6810-1b02-4d5e-a64b-66810ed9c14a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a351a2e-f550-41c5-a54d-1b7a4c577748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e4b6810-1b02-4d5e-a64b-66810ed9c14a",
            "compositeImage": {
                "id": "7193b97e-2627-461e-8c9c-482541d24296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a351a2e-f550-41c5-a54d-1b7a4c577748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1c3816-d641-4a31-8751-a6071b977c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a351a2e-f550-41c5-a54d-1b7a4c577748",
                    "LayerId": "b06da259-98bf-4cb3-8d0f-90acb14268e3"
                }
            ]
        },
        {
            "id": "45d92f7f-e2b5-4efb-90ff-5910fdbbeb44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e4b6810-1b02-4d5e-a64b-66810ed9c14a",
            "compositeImage": {
                "id": "beec3ab2-ff64-40a1-b932-ec391befbad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d92f7f-e2b5-4efb-90ff-5910fdbbeb44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b59d5e3c-f21f-4792-9887-c0014b00fa01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d92f7f-e2b5-4efb-90ff-5910fdbbeb44",
                    "LayerId": "b06da259-98bf-4cb3-8d0f-90acb14268e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b06da259-98bf-4cb3-8d0f-90acb14268e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e4b6810-1b02-4d5e-a64b-66810ed9c14a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}