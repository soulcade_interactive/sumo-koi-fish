{
    "id": "07f69f2f-b82a-4f5d-a589-25d99d8945a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fish_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 242,
    "bbox_left": 84,
    "bbox_right": 172,
    "bbox_top": 133,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb2326c0-c28a-401b-bd5b-63a970947a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f69f2f-b82a-4f5d-a589-25d99d8945a4",
            "compositeImage": {
                "id": "6bb3bd68-eca0-4796-ab99-b688d6bde75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb2326c0-c28a-401b-bd5b-63a970947a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8505d898-c635-4320-9142-fcf8578bc48e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb2326c0-c28a-401b-bd5b-63a970947a8e",
                    "LayerId": "78b862d4-d366-4e1c-baae-505e51177acc"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 256,
    "layers": [
        {
            "id": "78b862d4-d366-4e1c-baae-505e51177acc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07f69f2f-b82a-4f5d-a589-25d99d8945a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 160
}