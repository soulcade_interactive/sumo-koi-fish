{
    "id": "72c8a69b-46d6-42b3-899c-88fc108a8dec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_COM",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e4e3631-ea8d-4ca1-b0ce-7e3434b5488e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72c8a69b-46d6-42b3-899c-88fc108a8dec",
            "compositeImage": {
                "id": "8ddd6330-2b0b-4b8e-a847-3ca285bdb6bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4e3631-ea8d-4ca1-b0ce-7e3434b5488e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d658ebd2-4092-4473-89d9-c9a6df9c40eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4e3631-ea8d-4ca1-b0ce-7e3434b5488e",
                    "LayerId": "cfd7696e-edf4-4663-a3c6-3d105f141449"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "cfd7696e-edf4-4663-a3c6-3d105f141449",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72c8a69b-46d6-42b3-899c-88fc108a8dec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}