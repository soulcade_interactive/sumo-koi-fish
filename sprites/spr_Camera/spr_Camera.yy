{
    "id": "0535906d-7491-4b38-b97a-4b4873f3792f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Camera",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b18f47f-f09b-4631-bc1a-43051760212b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0535906d-7491-4b38-b97a-4b4873f3792f",
            "compositeImage": {
                "id": "a4c769a1-468a-4e04-9c6d-a0a21cd0419f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b18f47f-f09b-4631-bc1a-43051760212b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a65c75df-01a2-4d47-b40c-e4c804fe9230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b18f47f-f09b-4631-bc1a-43051760212b",
                    "LayerId": "70fc1e4c-dcdb-4384-a27a-080d6398da88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "70fc1e4c-dcdb-4384-a27a-080d6398da88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0535906d-7491-4b38-b97a-4b4873f3792f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}