{
    "id": "acf22290-6981-48d1-98dd-4a899841fbc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tex_sand_seamless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 893,
    "bbox_left": 0,
    "bbox_right": 893,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0736cba-4105-4155-8565-8995a80bbbd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acf22290-6981-48d1-98dd-4a899841fbc2",
            "compositeImage": {
                "id": "30b931ef-c0f9-4da8-823d-0a6cafe908c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0736cba-4105-4155-8565-8995a80bbbd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db889d7-bde6-40ac-85d2-01807d42a0ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0736cba-4105-4155-8565-8995a80bbbd6",
                    "LayerId": "6c204e73-2c5a-4615-a2be-57a7883fbfda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 894,
    "layers": [
        {
            "id": "6c204e73-2c5a-4615-a2be-57a7883fbfda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acf22290-6981-48d1-98dd-4a899841fbc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 894,
    "xorig": 447,
    "yorig": 447
}