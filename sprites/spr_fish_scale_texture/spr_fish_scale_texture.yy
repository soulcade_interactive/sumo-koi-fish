{
    "id": "afc2e205-51c9-4479-9e06-8960722d5b19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fish_scale_texture",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "00551d41-a7ab-4c8e-b60a-77345bc03876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afc2e205-51c9-4479-9e06-8960722d5b19",
            "compositeImage": {
                "id": "9d00b609-bb1d-447d-a8fb-1d79e378f626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00551d41-a7ab-4c8e-b60a-77345bc03876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4745f6f-4148-4505-ac92-763c5cadd5bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00551d41-a7ab-4c8e-b60a-77345bc03876",
                    "LayerId": "0e57ab55-2e0f-4676-8583-f8f1111482b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0e57ab55-2e0f-4676-8583-f8f1111482b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afc2e205-51c9-4479-9e06-8960722d5b19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}