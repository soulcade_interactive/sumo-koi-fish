{
    "id": "e1cb2a36-0288-4116-a8d9-cdd1738f8fd9",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_COM",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "31fd7529-c79a-4718-bea8-2afb4add910d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 57,
                "y": 53
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2bbf6ea0-3261-420c-b3c6-e36fa47e661a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 122,
                "y": 53
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f49019f6-dce0-46db-93b1-0123c9126950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "487b098d-076d-42cf-88d4-1a09a0681661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7845ca2f-e763-4b26-ba98-ad86bcb10ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "37b73194-4d59-49a5-bdbb-720debe52051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "aceb4798-0d4d-4c65-b079-41a022d7f3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7a6996cc-7301-490f-a0bf-92af005cad10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 102,
                "y": 53
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5e4920f2-b0c9-4c46-912b-88bf8f6e6f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 51,
                "y": 53
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "076e5ed9-5996-417e-abca-9dfc41fefbca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 81,
                "y": 53
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a6f3c9aa-14ce-473f-92e6-732731f0786c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 53
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7e8080da-d1b4-446a-ba04-416fec6f0b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 36
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4b45d8d3-8571-441a-9bd9-6ed8446eaeb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 92,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0de82cd9-d1b2-41c6-89d9-7009b44d8a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1c10693e-c797-43e1-bd66-c76a4624dc8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 107,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "857932d1-ee86-41e7-85bb-ccb8ef43f4df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 242,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9ecb8b50-76a9-48a2-9e9e-c4bf7419cc8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 122,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c0ecbd51-362e-41f3-8da8-f517a77ef8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 228,
                "y": 36
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c1dbb292-a543-42a8-b215-ad773812d41c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 95,
                "y": 36
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0130ecc9-2b6f-4f0e-9a4b-02f1aa753327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 113,
                "y": 36
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "71daee91-c432-4377-bad0-b7841a75729d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 140,
                "y": 36
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9f95043e-d4d4-4c03-a885-fb907ee407a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 131,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0c0d3e84-df0f-4d2f-9e1f-41c828f9d4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 158,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0a1fd0e7-c66f-4408-8921-6f79834c1e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 167,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "116ed502-2778-4eec-80b1-dacd513aa318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 185,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2db82fd6-a064-4d94-825c-eaacf8eb96a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 203,
                "y": 36
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d18cbca7-f5d9-4590-85b0-c97c5d9d4ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 112,
                "y": 53
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c7d44f9b-467d-42e1-881c-7c217eeb9c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 87,
                "y": 53
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b20b714e-9778-44de-8071-f3fa6b6be9eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 149,
                "y": 36
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8e3dba69-c11e-4514-8940-cd26540347e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 36
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ec577b76-b230-46b2-9035-a04b32a2b6e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 194,
                "y": 36
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ad809559-3a8b-47e8-9334-70af7790f88d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 238,
                "y": 19
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1930e61e-5db1-4aff-9297-bdc6e2fb0130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6d6c99ea-7a16-4e3c-b7d0-10ce89d2ea7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "35c3f87d-b9a0-4332-b866-0790a6c04637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "896c11e4-0e6a-46d5-bd69-71e2c0c69070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4d929771-ba14-4a46-9127-35d80c3b8ec9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c94eff2d-4ab6-479c-a9cc-ab1292dcce0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "011e6d59-a35d-4cd2-8179-659a4d004620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 228,
                "y": 19
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0df5b71e-fdc2-4c35-b38e-eea3963b60e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1a82e3d3-b24a-4ba1-89e7-69fe8b8005a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 19
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d7834b68-56b2-4d6b-8244-2117429cc2a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 117,
                "y": 53
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d9cddda6-cfe2-40c5-828e-9d23c997842a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "052b65eb-71bc-465a-90e4-be3e27c25fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fc941f1c-a436-488c-9b0d-f66ccc746510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 176,
                "y": 36
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "34a27107-9f1d-4d98-ae98-721fa14711b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f50d52c7-cf53-4399-b1ae-a24eb9c9ac5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "21f90635-abd6-4867-b94d-a7e1099eb128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "52a23598-05a6-46c7-a85f-ebf1b682ba64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a2dec459-c97e-450d-9ab8-72bbcd025264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e22c12e3-6fb6-425c-95ea-f3fd9d176b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8b2280e8-e3dd-48cf-abc6-8d49b8382601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a3339b2c-f18e-4f1b-8494-80fe7ff54fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 188,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7a0b921a-107e-459a-be9c-89aa70b3e91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "312bd7eb-fe8c-42b6-82cf-9be5a8e56ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b1f918a9-b5e2-44a2-ad49-39d666c90651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b7749d21-949a-4c09-ab8a-dd484f9584f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 19
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e31d7426-ae09-487c-8912-faedd63ea957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fb829268-f9f7-41d8-af4c-c155a0acce97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 138,
                "y": 19
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ee1281be-cbd4-41af-8ca6-e03f50da2320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 53
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a3c6e12b-e5ef-42ba-b360-f2bded101276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 30,
                "y": 53
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "fb1a60f9-de96-482d-abb4-527c232cf471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 75,
                "y": 53
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f2d90cbd-29b5-4893-a2c0-6cc23c5ad2c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 104,
                "y": 36
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "38abdddc-33a0-4fe8-a3cd-0337a8d1ef73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 57,
                "y": 19
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4332f1f5-7a0e-42f1-9712-1407c1c2cb6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 69,
                "y": 53
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8d04eed2-bd01-490a-a696-1858e8b9c541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 86,
                "y": 36
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "17e31383-c99b-4f6a-acf5-972f78741231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 108,
                "y": 19
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2a249706-d0de-447d-a3fe-6c2ea74d86ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 77,
                "y": 36
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cb8cf650-f9f3-4954-9c4e-b873454847c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 19
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ebaf5e71-4ce3-4403-a1c2-460deade80c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 68,
                "y": 36
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d1abb061-4a21-4d7f-96a2-51cd99b16f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 235,
                "y": 36
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c0ffbc92-162f-4694-89b8-4a787a3a0840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 88,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1f163c24-0e97-4332-9546-62a1bdc1030d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "091c402c-11e1-431f-a74f-a3d8e47491de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 97,
                "y": 53
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "676613dd-c73c-43db-a7c5-70d4c90749f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 53
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ba5c4bbe-8d23-45cb-8ad8-cf362cd76ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 208,
                "y": 19
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "963d04cc-acbe-46b1-9baa-17677ef84882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 127,
                "y": 53
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bb8e09f9-1ed5-4e36-b9f2-68ec248c48fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ac04a426-6e36-4c11-a804-86e6f0c21cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 178,
                "y": 19
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3fddf7d9-c7bb-4e3b-b313-8372b74aaf03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 148,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "97514202-dbd0-4c50-8266-37154e4968a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 198,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1afd56bb-8414-4b38-b639-f77d7157fad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 168,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b400e535-71e7-4b0e-8da8-297c05a04ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 212,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b237fff7-2047-4d7f-b120-422c17e654f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 59,
                "y": 36
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ac4c8c54-9474-44ae-ac17-aaf920d314fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 53
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "39676061-37a0-4472-9d1b-d2016f6853b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 158,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "21c054ca-9998-4426-831f-613668343e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 128,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "434caf20-ebc8-44fa-80bc-8a1126d21332",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8bc2d056-58e9-479d-b105-728f58d8c0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 118,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2295a909-9a6e-4e01-b7bd-9b5a97db4d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 68,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2d1e94a5-293e-427a-b7f9-98006dba8965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e0199f7d-e446-45f3-a4b9-482ac1ac6997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 53
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4380ec50-a7ef-4b0d-98c1-d9c0de259cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 132,
                "y": 53
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f32775fa-cbb0-48df-a527-12b37326ad1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "122c7df9-3ab9-4e9d-b76d-2358c2e9bfe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 218,
                "y": 19
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "546a3b79-3bcf-47dd-b0d0-ee0ddbf610c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "f18328a9-0f5a-4098-b99d-3598f23a9c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "e4b8a142-fb8f-40ce-a3ad-c431604f5c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "151de203-8a70-4ff7-adc8-df3ca66775b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "1a2df680-2adb-444a-b4ca-c25f6dda7ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "9c3259bd-ad07-4c59-8619-66fdc991cbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "c8295bdf-de39-4a9f-b3fc-24882d6795d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "2671ab1c-b107-4529-884b-c61a8c00c1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "c6df52f6-9905-43cd-b14a-62a1003bc43e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "e40ed147-af42-4082-b6cc-075447916f9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "3b3c4d0c-efd2-44bc-a357-9747422067dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "57c677b5-e908-4b81-b70b-39521f18e8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "4c589cf4-0b01-4730-9ffb-6956f7b0ae0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "9f93b8e5-6a41-4c3c-b5b1-6d72e8f83688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "b1f32646-8f8c-44ff-a7af-60b3d54ea156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "c7220dbb-c572-4c52-af5b-3ca5abdbe1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "7161fb7d-7b17-42b0-9e17-e7512c3d4173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "b48652ff-b28d-499e-82b8-4bfbce9c7238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "d7f46e3d-1a25-4760-8ed5-836ed826d0d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "154026d2-8b15-4a4f-a830-43049e5be1dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "4844539f-ebf5-4c2c-94ee-8d9a83ecfc67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "7fb365d6-48cd-49b3-8ae5-1eec39c94269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "0a70ae93-8877-4d58-b8ca-744ff8951c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "7ee45272-9448-49fb-9e22-9f72e2ce5d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "e1e8d46f-1e35-4524-944c-687033cd5b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "2b13aec8-af24-4f9f-99f0-601a645c8be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "74f6eafb-a0ec-4c6b-9184-3e7dec00a4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "8e8caaae-c58f-49b1-a5a1-b2b01cc4fcfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "27c77023-b1ed-4b50-858b-786801a8a9dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "faa3f0d5-1f47-4f15-b863-667d062dc2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "0b97775a-ad46-4464-b33b-d749d2acc045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "d235b2e7-e03e-4353-a470-cab30a8e714c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "0849f0fb-28f2-4b44-bead-a17d9362f4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "13999e2c-0a5c-43d1-88b8-5bb591939bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "396fb8c8-7549-495c-b499-aa1cdc974105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "51358964-49d7-4cb4-a65f-b819c55e291f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "c254b578-188d-4232-87cd-e0a7c4a64a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "0519a659-cc1d-45fc-9b29-b7334eed197f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "ee0daeb8-7732-4ffc-b2ce-b88acf945334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "7e17aa51-d8f4-4baf-b222-7f2bc2e7ecfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "9bb4b8d3-908d-4c8f-8054-f859fab10429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}