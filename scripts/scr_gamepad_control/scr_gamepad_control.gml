/// @description All Car Controls for Gamepad
/// @param Controlller Slot
//Bind function to slot
	var slot = argument0;

///Player Input
if gamepad_is_connected(slot)
   {
   
//Restart Game when Start is Pressed
	if gamepad_button_check_pressed(slot,gp_start)
		{
		room_restart();
		}
		
//Steering
   if phy_speed != 0
      {
	  var turn_angle = 6;
	  var rotate = (turn_angle * gamepad_axis_value(slot,gp_axislh)) + 1;
	  
      if gamepad_axis_value(slot,gp_axislh) < -0.5
         {
         phy_rotation += rotate
         }
   
   if gamepad_axis_value(slot,gp_axislh) > 0.5
       {
       phy_rotation +=  rotate
	   
       }
       }
	   	
//Acceleration and Boost
	   dash = (gamepad_button_check(slot,gp_face1));
       throttle = (gamepad_button_value(slot,gp_shoulderrb) + dash)*5000;
	   
	   if throttle > 0.1
          {
          forward_x = lengthdir_x(_speed,-phy_rotation - 90) * throttle;
          forward_y = lengthdir_y(_speed,-phy_rotation - 90) * throttle;
   
          physics_apply_force(phy_position_x,phy_position_y,forward_x,forward_y)
		  }

/*
		dash = (gamepad_button_check(slot,gp_face1))
		if (dash)
			{
			forward_x = lengthdir_x(_speed * 1.5,-phy_rotation - 90) * (dash*360);
			forward_y = lengthdir_y(_speed * 1.5,-phy_rotation - 90) * (dash*360);
   
          physics_apply_force(phy_position_x,phy_position_y,forward_x,forward_y)
			}
*/
//Braking
    brake = gamepad_button_value(slot,gp_shoulderlb)
    if brake > 0.1
    && !gamepad_button_check(slot,gp_face2)
       {
       phy_speed_x = lerp(phy_speed_x,0,0.03 * (brake));
       phy_speed_y = lerp(phy_speed_y,0,0.03 * (brake));
       } 


}
