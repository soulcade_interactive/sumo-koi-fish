uniTime = shader_get_uniform(sh_water, "Time");
uniTexel = shader_get_uniform(sh_water, "Texel");

shader_set(sh_water)

shader_set_uniform_f(uniTime, current_time);
var tex = surface_get_texture(application_surface);
shader_set_uniform_f(uniTexel, texture_get_texel_width(tex), texture_get_texel_height(tex));