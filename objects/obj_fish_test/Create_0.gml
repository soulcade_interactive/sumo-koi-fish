/// @description Insert description here

// Gameplay variables
controller_id = 0;
dp = 0;

//Define body properties for fish
fish_body_color = c_white;
fish_fin_color = c_gray;
body_length = random_range(152,256);
body_width = 76;
tail_segment_length = 4;
segment_count = 20;
spine_fin_segment_start = 3;
spine_fin_segment_end = 15;
spine_fin_segemental_overflow = 1;

//Define variables for procedurally generated segments of fish body
for(var i = 0; i < segment_count; i++)
	{
	var segment_percent = (segment_count - i)/segment_count;
	segment_width[i] = body_width * segment_percent;
	segment_angle[i] = 0;
	segment_length[i] = body_length/segment_count;
	segment_end_x[i] = 0;
	segment_end_y[i] = 0;
	segment_left_side_x[i] = 0;
	segment_left_side_y[i] = 0;
	segment_right_side_x[i] = 0;
	segment_right_side_y[i] = 0;
	}

spine_fin_x = 0;
spine_fin_y = 0;

tail_fin_x = 0;
tail_fin_y = 0;

tail_left_fin_angle = 0;
tail_right_fin_angle = 0;

tail_left_fin_x = 0;
tail_left_fin_y = 0;

tail_right_fin_x = 0;
tail_right_fin_y = 0;
show_debug_overlay(true);

//Define Physics properites for fish
var fixture_main_width = body_width/2;
var fixture_ends_width = body_width/4;
fixture_id = physics_fixture_create();
physics_fixture_set_polygon_shape(fixture_id);
physics_fixture_add_point(fixture_id, -fixture_ends_width,-80);
physics_fixture_add_point(fixture_id, fixture_ends_width,-80);
physics_fixture_add_point(fixture_id, fixture_main_width,0);
physics_fixture_add_point(fixture_id, fixture_ends_width,80);
physics_fixture_add_point(fixture_id, -fixture_ends_width,80);
physics_fixture_add_point(fixture_id, -fixture_main_width,0);
physics_fixture_set_density(fixture_id, 1);
physics_fixture_set_restitution(fixture_id,2);
physics_fixture_set_collision_group(fixture_id,0);
physics_fixture_set_linear_damping(fixture_id, 0.75);
physics_fixture_set_angular_damping(fixture_id,20);
physics_fixture_set_friction(fixture_id,0.8)
physics_fixture_set_awake(fixture_id,true);
fixture_index = physics_fixture_bind(fixture_id,id);
physics_fixture_delete(fixture_id);

//Fish Texture
var fish_texture = sprite_get_texture(spr_fish_scale_texture,0);

//Gameplay variables that tie directly to the physics
forward_x = 0;
forward_y = 0;
throttle = 0;
_speed = 9600;
physics_world_update_speed(60);
physics_world_update_iterations(10);

//Sets point for first segment of the body
	segment_angle[0] = -phy_rotation+90;
	segment_length[0] = body_length/segment_count;
	segment_end_x[0] = lerp(segment_end_x[0],phy_position_x + lengthdir_x(segment_length[0],segment_angle[0]),0.97);
	segment_end_y[0] = lerp(segment_end_y[0],phy_position_y + lengthdir_y(segment_length[0],segment_angle[0]),0.97);
	segment_left_side_x[0] = segment_end_x[0] + lengthdir_x(segment_width[0]/2,segment_angle[0]-90);
	segment_left_side_y[0] = segment_end_y[0] + lengthdir_y(segment_width[0]/2,segment_angle[0]-90);
	segment_right_side_x[0] = segment_end_x[0] + lengthdir_x(segment_width[0]/2,segment_angle[0]+90);
	segment_right_side_y[0] = segment_end_y[0] + lengthdir_y(segment_width[0]/2,segment_angle[0]+90);

//Sets points for the remainder of the body
for(var i = 1; i < segment_count; i++)
	{
	segment_angle[i] = lerp(segment_angle[i],segment_angle[i-1],0.95);
	segment_length[i] = body_length/segment_count;
	segment_end_x[i] = segment_end_x[i-1] + lengthdir_x(segment_length[i],segment_angle[i]);
	segment_end_y[i] = segment_end_y[i-1] + lengthdir_y(segment_length[i],segment_angle[i]);
	segment_left_side_x[i] = segment_end_x[i] + lengthdir_x(segment_width[i]/2,segment_angle[i]-90);
	segment_left_side_y[i] = segment_end_y[i] + lengthdir_y(segment_width[i]/2,segment_angle[i]-90);
	segment_right_side_x[i] = segment_end_x[i] + lengthdir_x(segment_width[i]/2,segment_angle[i]+90);
	segment_right_side_y[i] = segment_end_y[i] + lengthdir_y(segment_width[i]/2,segment_angle[i]+90);
	}
	
//Tail Code
var tail_lerp = 0.9;

//Set top tip coordinates of spinal fin
spine_fin_x = lerp(spine_fin_x,segment_end_x[spine_fin_segment_end],tail_lerp);
spine_fin_y = lerp(spine_fin_y,segment_end_y[spine_fin_segment_end],tail_lerp);

//set tail fin coordinates
tail_fin_x = lerp(tail_fin_x,segment_end_x[segment_count-1],tail_lerp);
tail_fin_y = lerp(tail_fin_y,segment_end_y[segment_count-1],tail_lerp);

//set left and right fin angles
tail_left_fin_angle = lerp(tail_left_fin_angle,segment_angle[segment_count-1]-45,tail_lerp);
tail_right_fin_angle =  lerp(tail_right_fin_angle,segment_angle[segment_count-1]+45,tail_lerp);

//set left fin coordinates
tail_left_fin_x = lerp(tail_left_fin_x,segment_end_x[segment_count-1] + lengthdir_x(segment_length[segment_count-1] * tail_segment_length,tail_left_fin_angle),tail_lerp);
tail_left_fin_y = lerp(tail_left_fin_y,segment_end_y[segment_count-1] + lengthdir_y(segment_length[segment_count-1] * tail_segment_length,tail_left_fin_angle),tail_lerp);

//set right fin coordinates
tail_right_fin_x = lerp(tail_right_fin_x,segment_end_x[segment_count-1] + lengthdir_x(segment_length[segment_count-1] * tail_segment_length,tail_right_fin_angle),tail_lerp);
tail_right_fin_y = lerp(tail_right_fin_y,segment_end_y[segment_count-1] + lengthdir_y(segment_length[segment_count-1] * tail_segment_length,tail_right_fin_angle),tail_lerp);
