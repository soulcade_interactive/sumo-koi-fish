{
    "id": "e1a6e474-a727-4146-a640-21c0cb7a74e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fish_test",
    "eventList": [
        {
            "id": "2f0ed263-f6bf-441c-8441-3a80448c380e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1a6e474-a727-4146-a640-21c0cb7a74e3"
        },
        {
            "id": "77b1114f-4098-478a-8282-4d4872474f54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e1a6e474-a727-4146-a640-21c0cb7a74e3"
        },
        {
            "id": "07bb65e2-c492-4717-8cc7-4c64150c0246",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "e1a6e474-a727-4146-a640-21c0cb7a74e3"
        },
        {
            "id": "cb58effb-0737-4eec-b6ff-e7fc925aa073",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e1a6e474-a727-4146-a640-21c0cb7a74e3"
        },
        {
            "id": "45409dbd-e93c-4dff-bdb1-a33b5f531159",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e1a6e474-a727-4146-a640-21c0cb7a74e3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e1a6e474-a727-4146-a640-21c0cb7a74e3"
        },
        {
            "id": "2a620f4d-39ee-4e31-b672-7ccf21b0b27f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e1a6e474-a727-4146-a640-21c0cb7a74e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 6,
    "physicsDensity": 1,
    "physicsFriction": 0.8,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.75,
    "physicsObject": false,
    "physicsRestitution": 2,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "7b4afd66-e711-4f71-804d-0e2dac05f9e9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 142,
            "y": 62.5
        },
        {
            "id": "3134454b-56de-4fdf-91a8-176c511d691c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 172,
            "y": 163.5
        },
        {
            "id": "06223161-6cf8-4cd9-b88d-a4561f2227e4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 152,
            "y": 234.5
        },
        {
            "id": "452adbbd-4df8-4ede-8225-0d12980f7efa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 101,
            "y": 233.5
        },
        {
            "id": "5c998ef9-90b8-4fc5-a06d-8f2a305670ff",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 83,
            "y": 162.5
        },
        {
            "id": "f4f88ba4-a321-414d-801b-265a8df51815",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 112,
            "y": 62.5
        }
    ],
    "physicsStartAwake": false,
    "properties": null,
    "solid": false,
    "spriteId": "07f69f2f-b82a-4f5d-a589-25d99d8945a4",
    "visible": true
}