/// @description Insert description here
// You can write your code in this editor
if (live_call()) return live_result;

draw_set_alpha(0.5)
draw_self();
draw_set_alpha(1)
//Draw Tail Fins
draw_set_color(c_gray);
//Top Fin
draw_primitive_begin(pr_trianglestrip);
for(var i = segment_count - (tail_segment_length + 1); i < segment_count; i++)
	{
	draw_vertex(segment_end_x[i],segment_end_y[i]);
	draw_vertex(tail_fin_x,tail_fin_y);
	}
draw_primitive_end();
//Left Fin
draw_primitive_begin(pr_trianglestrip);
for(var i = segment_count - (tail_segment_length + 1); i < segment_count; i++)
	{
	draw_vertex(segment_end_x[i],segment_end_y[i]);
	draw_vertex(tail_left_fin_x,tail_left_fin_y);
	}
draw_primitive_end();
//Right Fin
draw_primitive_begin(pr_trianglestrip);
for(var i = segment_count - (tail_segment_length + 1); i < segment_count; i++)
	{
	draw_vertex(segment_end_x[i],segment_end_y[i]);
	draw_vertex(tail_right_fin_x,tail_right_fin_y);
	}
draw_primitive_end();

//Draw Body
draw_set_color(c_white);
var fish_texture = sprite_get_texture(spr_fish_scale_texture,0);
draw_primitive_begin_texture(pr_trianglestrip,fish_texture);
for(var i = 0; i < segment_count; i++)
	{
	draw_vertex_texture(segment_left_side_x[i],segment_left_side_y[i],0,i/segment_count);
	draw_vertex_texture(segment_right_side_x[i],segment_right_side_y[i],1,i/segment_count);
	}
draw_primitive_end();

draw_set_color(c_gray);

//Draw Spine Fin
draw_primitive_begin(pr_trianglestrip);
for(var i = spine_fin_segment_start; i < spine_fin_segment_end - spine_fin_segemental_overflow; i++)
	{
	draw_vertex(segment_end_x[i],segment_end_y[i]);
	draw_vertex(spine_fin_x,spine_fin_y);
	}
draw_primitive_end();

//Draw Debug Skeleton
draw_set_color(c_green);
physics_draw_debug();

for(var i = 1; i < segment_count; i++)
	{
	draw_line(segment_end_x[i-1],segment_end_y[i-1],segment_end_x[i],segment_end_y[i]);
	draw_line(segment_end_x[i],segment_end_y[i],segment_left_side_x[i],segment_left_side_y[i]);
	draw_line(segment_end_x[i],segment_end_y[i],segment_right_side_x[i],segment_right_side_y[i]);
	}

draw_set_color(c_white);

//
draw_set_color(c_red);
draw_text(x,y,string(dp));