/// @description Calculate Segment Positions

//Sets point for first segment of the body
	segment_angle[0] = -phy_rotation+90;
	segment_length[0] = body_length/segment_count;
	segment_end_x[0] = lerp(segment_end_x[0],phy_position_x + lengthdir_x(segment_length[0],segment_angle[0]),0.7);
	segment_end_y[0] = lerp(segment_end_y[0],phy_position_y + lengthdir_y(segment_length[0],segment_angle[0]),0.7);
	segment_left_side_x[0] = segment_end_x[0] + lengthdir_x(segment_width[0]/2,segment_angle[0]-90);
	segment_left_side_y[0] = segment_end_y[0] + lengthdir_y(segment_width[0]/2,segment_angle[0]-90);
	segment_right_side_x[0] = segment_end_x[0] + lengthdir_x(segment_width[0]/2,segment_angle[0]+90);
	segment_right_side_y[0] = segment_end_y[0] + lengthdir_y(segment_width[0]/2,segment_angle[0]+90);


//Sets points for the remainder of the body
for(var i = 1; i < segment_count; i++)
	{
	segment_angle[i] = lerp(segment_angle[i],segment_angle[i-1],0.5);
	segment_length[i] = body_length/segment_count;
	segment_end_x[i] = segment_end_x[i-1] + lengthdir_x(segment_length[i],segment_angle[i]);
	segment_end_y[i] = segment_end_y[i-1] + lengthdir_y(segment_length[i],segment_angle[i]);
	segment_left_side_x[i] = segment_end_x[i] + lengthdir_x(segment_width[i]/2,segment_angle[i]-90);
	segment_left_side_y[i] = segment_end_y[i] + lengthdir_y(segment_width[i]/2,segment_angle[i]-90);
	segment_right_side_x[i] = segment_end_x[i] + lengthdir_x(segment_width[i]/2,segment_angle[i]+90);
	segment_right_side_y[i] = segment_end_y[i] + lengthdir_y(segment_width[i]/2,segment_angle[i]+90);

	}

//Tail Code
var tail_lerp = 0.75;

//Set top tip coordinates of spinal fin
spine_fin_x = lerp(spine_fin_x,segment_end_x[spine_fin_segment_end],tail_lerp);
spine_fin_y = lerp(spine_fin_y,segment_end_y[spine_fin_segment_end],tail_lerp);

//set tail fin coordinates
tail_fin_x = lerp(tail_fin_x,segment_end_x[segment_count-1],tail_lerp);
tail_fin_y = lerp(tail_fin_y,segment_end_y[segment_count-1],tail_lerp);

//set left and right fin angles
tail_left_fin_angle = lerp(tail_left_fin_angle,segment_angle[segment_count-1]-45,tail_lerp);
tail_right_fin_angle =  lerp(tail_right_fin_angle,segment_angle[segment_count-1]+45,tail_lerp);

//set left fin coordinates
tail_left_fin_x = lerp(tail_left_fin_x,segment_end_x[segment_count-1] + lengthdir_x(segment_length[segment_count-1] * tail_segment_length,tail_left_fin_angle),tail_lerp);
tail_left_fin_y = lerp(tail_left_fin_y,segment_end_y[segment_count-1] + lengthdir_y(segment_length[segment_count-1] * tail_segment_length,tail_left_fin_angle),tail_lerp);

//set right fin coordinates
tail_right_fin_x = lerp(tail_right_fin_x,segment_end_x[segment_count-1] + lengthdir_x(segment_length[segment_count-1] * tail_segment_length,tail_right_fin_angle),tail_lerp);
tail_right_fin_y = lerp(tail_right_fin_y,segment_end_y[segment_count-1] + lengthdir_y(segment_length[segment_count-1] * tail_segment_length,tail_right_fin_angle),tail_lerp);
