ControllerID = noone;
WhoAmI = noone;

image_speed = 0;

KeyLeft = 0;
KeyRight = 0;
KeyUp = 0;
KeyDown = 0;

Speed = 200;

// Geometry
dx = sprite_get_width(spr_Player) / 2;
dy = sprite_get_height(spr_Player) / 2;

// Physics
var Fixture = physics_fixture_create(),
    Group = 0,
    Density = 0.1,
    Restitution = 0.2,
    Friction = 20,
    LinearDamp = 4,
    AngularDamp = 0;
    
physics_fixture_set_circle_shape(Fixture, dx);

physics_fixture_set_collision_group(Fixture, Group);
physics_fixture_set_density(Fixture, Density);
physics_fixture_set_restitution(Fixture, Restitution);
physics_fixture_set_friction(Fixture, Friction);
physics_fixture_set_linear_damping(Fixture, LinearDamp);
physics_fixture_set_angular_damping(Fixture, AngularDamp);

physics_fixture_bind(Fixture, id);
physics_fixture_delete(Fixture);

