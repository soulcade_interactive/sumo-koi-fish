switch (WhoAmI)
{
    // Red Player
    case "P1":
    {
        KeyLeft = -keyboard_check(ord("A"));
        KeyRight = keyboard_check(ord("D"));
        KeyUp = -keyboard_check(ord("W"));
        KeyDown = keyboard_check(ord("S"));
        
        physics_apply_local_force(0, 0, (KeyLeft + KeyRight) * Speed, (KeyUp + KeyDown) * Speed);
        break;
    }
    
    // Green Player
    case "P2":
    {
        KeyLeft = -keyboard_check(vk_left);
        KeyRight = keyboard_check(vk_right);
        KeyUp = -keyboard_check(vk_up);
        KeyDown = keyboard_check(vk_down);
        
        physics_apply_local_force(0, 0, (KeyLeft + KeyRight) * Speed, (KeyUp + KeyDown) * Speed);
        break;
    }
}

phy_angular_velocity = 0;
phy_rotation = 0;

