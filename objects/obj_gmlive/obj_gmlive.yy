{
    "id": "33634c94-b392-4387-9e8a-14a6330a1256",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gmlive",
    "eventList": [
        {
            "id": "29b722f9-1377-41eb-b5b7-2cc8d7ce2799",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "5ce54531-4eb6-4c35-974b-58a9bb389589",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "4e84e70b-1f0d-4822-9852-4449340dc9c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "86abfbc2-ae41-4d59-b65f-5e2092f73b22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}