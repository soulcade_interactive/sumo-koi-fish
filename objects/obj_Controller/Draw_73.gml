
draw_set_alpha(1);
draw_set_colour(c_white);

draw_sprite(spr_COM, 0, x_COM, y_COM);
draw_sprite(spr_Camera, 0, x_Camera, y_Camera);

draw_set_colour(c_blue);
draw_set_font(fnt_COM);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_text(x_COM, y_COM - Offset_COM, string_hash_to_newline("COM"));

draw_set_colour(c_red);

draw_text(x_Camera, y_Camera + Offset_COM, string_hash_to_newline("Camera"));

