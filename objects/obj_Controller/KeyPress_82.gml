/// @description  Game Restart

with (all)
{
    instance_destroy();
}

room_restart();

