
draw_set_alpha(1);

// Zoom out boundary

var X1 = x_Zoom - x_ZoomOut/2,
    Y1 = y_Zoom - y_ZoomOut/2,
    X2 = x_Zoom + x_ZoomOut/2,
    Y2 = y_Zoom + y_ZoomOut/2,

draw_set_colour(c_blue);

draw_line_width(X1, Y1, X2, Y1, LineWidth);
draw_line_width(X1, Y1, X1, Y2, LineWidth);
draw_line_width(X2, Y1, X2, Y2, LineWidth);
draw_line_width(X1, Y2, X2, Y2, LineWidth);

// Zoom in boundary

var X1 = x_Zoom - x_ZoomIn/2,
    Y1 = y_Zoom - y_ZoomIn/2,
    X2 = x_Zoom + x_ZoomIn/2,
    Y2 = y_Zoom + y_ZoomIn/2,

draw_set_colour(c_red);

draw_line_width(X1, Y1, X2, Y1, LineWidth);
draw_line_width(X1, Y1, X1, Y2, LineWidth);
draw_line_width(X2, Y1, X2, Y2, LineWidth);
draw_line_width(X1, Y2, X2, Y2, LineWidth);
