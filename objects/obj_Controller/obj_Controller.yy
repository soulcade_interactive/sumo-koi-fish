{
    "id": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Controller",
    "eventList": [
        {
            "id": "052cc019-1e5e-423f-b075-54a4a86328bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76"
        },
        {
            "id": "ee53fd74-607a-49d1-9990-88124bf0f2f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76"
        },
        {
            "id": "0cb8abb7-41b7-46a0-9aa2-148e03517a4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76"
        },
        {
            "id": "0267e8ae-990f-47bb-af4c-22cb1c8407a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76"
        },
        {
            "id": "2947d96b-e129-445c-b973-4eff47d2c9ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76"
        },
        {
            "id": "d4bdde67-3320-4231-9222-647bb20df526",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76"
        },
        {
            "id": "9cfc8419-6759-4762-83b3-ccdfd3373d11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "2fe1112a-3281-473e-bdfd-f6b27b8c5d76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}