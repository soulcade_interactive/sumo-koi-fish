// Calculate center of mass using all players' coordinates and check for (x, y) min / max 
x_COM = 0;
y_COM = 0;

x_Max = 0;
y_Max = 0;

for (var i = 0; i < Players; i++)
{
    // If using physics
    x_COM += PlayerID[i].phy_position_x;
    y_COM += PlayerID[i].phy_position_y;
    
    // If not using physics
    //x_COM += PlayerID[i].x;
    //y_COM += PlayerID[i].y;
    
    if (abs(PlayerID[i].phy_position_x - x_Zoom) > x_Max)
    {
        x_Max = abs(PlayerID[i].phy_position_x - x_Zoom);
        //x_Max = PlayerID[i].x - x_Zoom;
    }
    
    if (abs(PlayerID[i].phy_position_y - y_Zoom) > y_Max)
    {
        y_Max = abs(PlayerID[i].phy_position_y - y_Zoom);
        //y_Max = PlayerID[i].y - y_Zoom;
    }
}

// Camera Zooming
    // Zoom out
if (!point_in_rectangle(x_Max, y_Max, 0, 0, x_ZoomOut/2, y_ZoomOut/2))
{
    if (x_Max >= x_ZoomOut/2)
    {
        // Modified cubic easing in/out from http://gizma.com/easing/
        __view_set( e__VW.WView, 0, median(ZoomMin, ZoomMax, ZoomOutSpeed * ZoomOutSpeed * ZoomOutSpeed * (x_Max - x_ZoomOut/2)/2 + __view_get( e__VW.WView, 0 ) ));
        __view_set( e__VW.HView, 0, __view_get( e__VW.WView, 0 ) / ViewRatio );
    }
    if (y_Max >= y_ZoomOut/2)
    {
        // Modified cubic easing in/out from http://gizma.com/easing/
        __view_set( e__VW.HView, 0, median(ZoomMin / ViewRatio, ZoomMax / ViewRatio, ZoomOutSpeed * ZoomOutSpeed * ZoomOutSpeed * (y_Max - y_ZoomOut/2)/2 + __view_get( e__VW.HView, 0 ) ));
        __view_set( e__VW.WView, 0, __view_get( e__VW.HView, 0 ) * ViewRatio );
    }
    
    ZoomInitial = __view_get( e__VW.WView, 0 );
    ZoomCurrent = ZoomInitial;
}
    // Zoom in
else if (point_in_rectangle(x_Max, y_Max, 0, 0, x_ZoomIn/2, y_ZoomIn/2))
{
    if (x_Max / x_ZoomIn/2 >= y_Max / y_ZoomIn/2)
    {
        // Modified cubic easing in/out from http://gizma.com/easing/
        __view_set( e__VW.WView, 0, median(ZoomMin, ZoomMax, ZoomInSpeed * ZoomInSpeed * ZoomInSpeed * (x_Max - x_ZoomIn/2)/2 + __view_get( e__VW.WView, 0 ) ));
        __view_set( e__VW.HView, 0, __view_get( e__VW.WView, 0 ) / ViewRatio );
    }
    else
    {
        // Modified cubic easing in/out from http://gizma.com/easing/
        __view_set( e__VW.HView, 0, median(ZoomMin / ViewRatio, ZoomMax / ViewRatio, ZoomInSpeed * ZoomInSpeed * ZoomInSpeed * (y_Max - y_ZoomIn/2)/2 + __view_get( e__VW.HView, 0 ) ));
        __view_set( e__VW.WView, 0, __view_get( e__VW.HView, 0 ) * ViewRatio );
    }
}

// Update zoom boundaries to reflect changes in view size
x_ZoomOut = ZoomOutBoundary * __view_get( e__VW.WView, 0 );
y_ZoomOut = ZoomOutBoundary * __view_get( e__VW.HView, 0 );
x_ZoomIn = ZoomInBoundary * __view_get( e__VW.WView, 0 );
y_ZoomIn = ZoomInBoundary * __view_get( e__VW.HView, 0 );

// Center of mass
x_COM /= Players;
y_COM /= Players;

// Camera position
    // Modified cubic easing in/out from http://gizma.com/easing/
x_Camera = PanSpeed * PanSpeed * PanSpeed * (x_COM - x_Camera)/2 + x_Camera;
y_Camera = PanSpeed * PanSpeed * PanSpeed * (y_COM - y_Camera)/2 + y_Camera;

__view_set( e__VW.XView, 0, x_Camera - __view_get( e__VW.WView, 0 )/2 );
__view_set( e__VW.YView, 0, y_Camera - __view_get( e__VW.HView, 0 )/2 );

x_Zoom = __view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 )/2;
y_Zoom = __view_get( e__VW.YView, 0 ) + __view_get( e__VW.HView, 0 )/2;

