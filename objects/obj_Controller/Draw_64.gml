
draw_set_alpha(1);
draw_set_colour(c_white);
draw_set_font(fnt_COM);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

draw_text(4, 4, string_hash_to_newline("x_COM: " + string_format(x_COM, 5, 0)));
draw_text(4, 24, string_hash_to_newline("y_COM: " + string_format(y_COM, 5, 0)));

draw_text(4, 54, string_hash_to_newline("x_ZoomIn/2: " + string_format(x_ZoomIn/2, 5, 0)));
draw_text(4, 74, string_hash_to_newline("x_Max: " + string_format(x_Max, 5, 0)));
draw_text(4, 94, string_hash_to_newline("x_ZoomOut/2: " + string_format(x_ZoomOut/2, 5, 0)));

draw_text(4, 124, string_hash_to_newline("y_ZoomIn/2: " + string_format(y_ZoomIn/2, 5, 0)));
draw_text(4, 144, string_hash_to_newline("y_Max: " + string_format(y_Max, 5, 0)));
draw_text(4, 164, string_hash_to_newline("y_ZoomOut/2: " + string_format(y_ZoomOut/2, 5, 0)));

for (var i = 0; i < Players; i++)
	{
	var xx = (1920/5) * (i+1)
	var yy = 1080 * 75;
	
	draw_text(xx,yy,PlayerID[i].dp)
	}