

// Zoom in/out boundary thickness
LineWidth = 2;

x_Center = room_width/2;
y_Center = room_height/2;

// Initialize center of mass
x_COM = 0;
y_COM = 0;
Offset_COM = 12;

// Initialize zoom parameters
x_Max = 0;
y_Max = 0;
/*
// Physics
physics_world_create(1/10);
physics_world_gravity(0, 0);
physics_world_update_iterations(10);
physics_world_update_speed(room_speed);
*/
// Create players
Players = 4;
//Spawn Distance
spawn_dist = 420;
dx_Spawn = 128;

for (var i = 0; i < Players; i++)
{
	
	var spawn_angle = (360/Players) * i;
	var spawn_x = room_width/2 + lengthdir_x(spawn_dist,spawn_angle);
	var spawn_y = room_height/2 + lengthdir_y(spawn_dist,spawn_angle);
	var spawned_direction = point_direction(spawn_x,spawn_y,room_width/2,room_height/2);
	
	PlayerID[i] = instance_create(spawn_x,spawn_y, obj_fish_test);
    //PlayerID[i] = instance_create(x_Center - (dx_Spawn * (Players - 1) / 2) + (dx_Spawn * i), y_Center, obj_fish_test);
    
    PlayerID[i].ControllerID = id;
    PlayerID[i].WhoAmI = "P" + string(i + 1);
	PlayerID[i].controller_id = i;
	PlayerID[i].phy_rotation = -spawned_direction - 90;
    
    // If using physics
    x_COM += PlayerID[i].phy_position_x;
    y_COM += PlayerID[i].phy_position_y;
    
    // If not using physics
    //x_COM += PlayerID[i].x;
    //y_COM += PlayerID[i].y;
}

// Calculate center of mass
x_COM /= Players;
y_COM /= Players;

// Camera
__view_set( e__VW.XView, 0, x_Center - __view_get( e__VW.WView, 0 )/2 );
__view_set( e__VW.YView, 0, y_Center - __view_get( e__VW.HView, 0 )/2 );
ViewRatio = __view_get( e__VW.WView, 0 ) / __view_get( e__VW.HView, 0 );                  // View ration ensures width and height will scale proportionally

// Panning parameters
x_Camera = __view_get( e__VW.XView, 0 ) + __view_get( e__VW.WView, 0 )/2;
y_Camera = __view_get( e__VW.YView, 0 ) + __view_get( e__VW.HView, 0 )/2;
PanSpeed = 0.6;                                             // Must be between 0 and 1; the larger the faster the pan speed

// Zoom parameters
x_Zoom = x_Camera;
y_Zoom = y_Camera;
ZoomOutSpeed = 0.6;                                         // Must be between 0 and 1; the larger the faster the zoom out speed
ZoomInSpeed = 0.8;                                          // Must be between 0 and 1; the larger the faster the zoom out speed
ZoomMax = room_width;
ZoomMin = __view_get( e__VW.WView, 0 );
ZoomOutBoundary = 0.6;                                      // Must be between 0 and 1; the larger the closer to the edge the player must travel before zooming out
x_ZoomOut = ZoomOutBoundary * __view_get( e__VW.WView, 0 );
y_ZoomOut = ZoomOutBoundary * __view_get( e__VW.HView, 0 );
ZoomInBoundary = 0.4                                        // Must be between 0 and 1; the smaller the closer to the center the player must travel before zooming in
x_ZoomIn = ZoomInBoundary * __view_get( e__VW.WView, 0 );
y_ZoomIn = ZoomInBoundary * __view_get( e__VW.HView, 0 );

